﻿using System.Threading;
using System.Threading.Tasks;

namespace Dk.MapPlanner.GenericRepository.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly IDbContext _context;

        public UnitOfWork(IDbContext context)
        {
            _context = context;
        }


        int IUnitOfWork.SaveChanges()
        {
            return _context.SaveChanges();
        }

        async Task<int> IUnitOfWork.SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        async Task<int> IUnitOfWork.SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        int IUnitOfWork.SaveChanges(bool acceptAllChangesOnSuccess)
        {
            return _context.SaveChanges(acceptAllChangesOnSuccess);
        }
    }
}