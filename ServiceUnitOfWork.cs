﻿using System.Threading;
using System.Threading.Tasks;

namespace Dk.MapPlanner.GenericRepository.Core
{
    public class ServiceUnitOfWork : ServiceUnitOfWorkBase
    {
        public ServiceUnitOfWork(IDbContext context) : base(context)
        {
        }

        protected int SaveChanges()
        {
            return _context.SaveChanges();
        }

        protected async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        protected int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            return _context.SaveChanges(acceptAllChangesOnSuccess);
        }
    }
}