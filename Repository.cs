﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Dk.MapPlanner.GenericRepository.Core
{
    public class Repository<TDbContext, TEntity> :
        IRepository<TEntity> where TEntity : class where TDbContext : DbContext
    {
        protected readonly TDbContext _context;

        public Repository(TDbContext context)
        {
            _context = context;
        }

        public virtual TEntity Get(params object[] keys)
        {
            return _context.Set<TEntity>().Find(keys);
        }

        public virtual IReadOnlyList<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }


        public virtual IReadOnlyList<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Where(predicate).ToList();
        }

        public virtual TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().FirstOrDefault(predicate);
        }

        public virtual void Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().AddRange(entities);
        }

        public virtual void Remove(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public virtual void RemoveRange(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().RemoveRange(entities);
        }

        public virtual void DisconnectedUpdate(TEntity entity)
        {
            _context.Set<TEntity>().Update(entity);
        }

        public virtual void DisconnectedInsert(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }


        public virtual async Task<TEntity> GetAsync(params object[] keys)
        {
            return await _context.Set<TEntity>().FindAsync(keys);
        }

        public virtual async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public virtual async Task<IReadOnlyList<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public virtual async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(predicate);
        }

        public virtual async Task AddAsync(TEntity entity)
        {
            await _context.Set<TEntity>().AddAsync(entity);
        }

        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            await _context.Set<TEntity>().AddRangeAsync(entities);
        }

        public virtual async Task RemoveAsync(TEntity entity)
        {
            await Task.Run(() => _context.Set<TEntity>().Remove(entity));
        }

        public virtual async Task RemoveRangeAsync(IEnumerable<TEntity> entities)
        {
            await Task.Run(() => _context.Set<TEntity>().RemoveRange(entities));
        }

        public virtual async Task DisconnectedUpdateAsync(TEntity entity)
        {
            await Task.Run(() => _context.Set<TEntity>().Update(entity));
        }

        public virtual async Task DisconnectedInsertAsync(TEntity entity)
        {
            await _context.Set<TEntity>().AddAsync(entity);
        }

        public int SaveChanges()
        {
            throw new Exception("SaveChanges is not allowed in repository!");
        }
    }
}