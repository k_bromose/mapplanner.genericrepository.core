﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Dk.MapPlanner.GenericRepository.Core
{
    public abstract class ServiceUnitOfWorkBase : IServiceUnitOfWork
    {
        protected readonly IDbContext _context;

        public ServiceUnitOfWorkBase(IDbContext context)
        {
            _context = context;
        }


        int IUnitOfWork.SaveChanges(bool acceptAllChangesOnSuccess)
        {
            throw new NotImplementedException();
        }

        int IUnitOfWork.SaveChanges()
        {
            throw new NotImplementedException();
        }

        Task<int> IUnitOfWork.SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        Task<int> IUnitOfWork.SaveChangesAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}