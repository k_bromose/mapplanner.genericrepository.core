﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Dk.MapPlanner.GenericRepository.Core
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(params object[] keys);
        IReadOnlyList<TEntity> GetAll();
        IReadOnlyList<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        void DisconnectedUpdate(TEntity entity);
        void DisconnectedInsert(TEntity entity);
        Task<TEntity> GetAsync(params object[] keys);
        Task<IReadOnlyList<TEntity>> GetAllAsync();
        Task<IReadOnlyList<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        Task AddAsync(TEntity entity);
        Task AddRangeAsync(IEnumerable<TEntity> entities);
        Task RemoveAsync(TEntity entity);
        Task RemoveRangeAsync(IEnumerable<TEntity> entities);
        Task DisconnectedUpdateAsync(TEntity entity);
        Task DisconnectedInsertAsync(TEntity entity);
    }
}