﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Dk.MapPlanner.GenericRepository.Core
{
    public interface IUnitOfWork
    {
        int SaveChanges(bool acceptAllChangesOnSuccess);
        int SaveChanges();
        Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken));
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}